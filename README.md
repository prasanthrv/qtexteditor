QTextEditor
==========

A text editor like Notepad for cross-platform use made using Qt.

It boasts a minimal but functional interface.

Running
=======

* Install [QtCreator](https://www.qt.io/ide/)
* Import project
* Set target as `Desktop`
* Run `qmake` from the `Build` menu
* Use the `Run` command from the `Build` menu
